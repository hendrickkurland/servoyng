
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A3033AA8-4E8F-4BC0-88BC-4A85A4262665"}
 */
function onShow(firstShow, event) {
	if (firstShow == true){
		elements.tabpanel_3.dividerLocation = 0.75 ;
	}
}
