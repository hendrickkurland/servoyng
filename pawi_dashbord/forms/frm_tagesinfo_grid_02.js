/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"138A970E-C641-4880-A7A0-FB1182CFCA13"}
 */
var lcAufriss = 'Rechnung' ;


/**
 * @properties={typeid:24,uuid:"4D839156-7AFE-4E2A-B85A-84554A7A6921"}
 */
function SqlBau(){
	

	
	var loParentForm = forms.frm_tagesinfo
	var lcKbezirk = loParentForm.lcKbezirk ;
	var lcBuper = loParentForm.lcBuper ;
	var lcBujahr = loParentForm.lcBujahr ;

	
	var loForm1 = forms.frm_tagesinfo_grid_01
	var lcPeriode = loForm1.lcPeriode ;
	var lcOffen = loForm1.lcOffen ;
	
	

	
	var laRows = loForm1.elements.datasettable_1.getSelectedRows()	
	if (laRows.length == 0){
		//... Keine Datensätze ausgewählt
		elements.datasettable_1.columns = []
		return ;
	}
	

	
	/**@type{Array<*>}*/
	var laParams = [lcBujahr,0] ;
	
	
	var lcSql = 'select ' ;
	
	switch (lcOffen){
	case 'gebucht' :
			
			switch (lcAufriss){
				case 'Rechnung' :
					lcSql += 'TRIM(LEADING "0" FROM v.vbeln) as vbeln,'
					lcSql += 'v.kunnr,'
					lcSql += 'concat(k.name1," ",k.ort01) as kunde_name, '
					lcSql += 'v.cpudt,'
					break ;
				case 'RechnungsPos' :
					lcSql += 'TRIM(LEADING "0" FROM v.vbeln) as vbeln,'
					lcSql += 'v.posnr,'
					lcSql += 'v.kunnr,'
					lcSql += 'concat(k.name1," ",k.ort01) as kunde_name, '
					lcSql += 'v.matnr,'
					lcSql += 'if (p.vbeln is null,m.maktd,p.arktx) as mat_bezeich,'
					lcSql += 'v.zz_mindx as mindx,'
					lcSql += 'if (p.vbeln is null,"",p.txt_0001) as txt_0001 ,'
					lcSql += 'if (p.vbeln is null,0,p.aumng)     as aumng ,'
					lcSql += 'if (p.vbeln is null,0,p.kwmeng)    as kwmeng,'
					lcSql += 'if (p.vbeln is null,0,p.kalab)     as kalab,'
					break ;
				case 'Kunde' :
					lcSql += 'v.kunnr,'
					lcSql += 'concat(k.name1," ",k.ort01) as kunde_name, '
					break ;
				case 'VAD' :
					lcSql += 'v.kbezirk,'
					break ;
			}

			lcSql += 'sum(v.netwr) as netwr '
			
			lcSql += 'from mes_vkumsatz v ' ;
			lcSql += 'inner join mes_kunden k on v.kunnr = k.kunnr '
			lcSql += 'left outer join mes_matstamm m on v.matnr = m.matnr '
			lcSql += 'left outer join mes_kdpos p on TRIM(LEADING "0" from v.vbelv) = p.vbeln and trim(leading "0" from v.posnv) = p.posnr '

			lcSql += 'WHERE v.gjahr = ? '				
			lcSql += 'AND v.netwr <> ? '
				
			
			
			
			if (!(lcBuper == null || lcBuper == 0)){
				lcSql += 'and v.buper = ? '
				laParams.push(lcBuper)
			}
			
			if (lcKbezirk && lcKbezirk.length > 0){
				lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
				laParams.push(lcKbezirk)
			}
			
			lcSql += 'AND ('
			for ( var i = 0 ; i < laRows.length ; i++ )	{
				if (i>0){
					lcSql += ' OR '
				}
				switch (lcPeriode){
					case 'Tag' :
						lcSql += ' v.cpudt = ? '
						var loRowData = laRows[i] ;
						var lcCpudt = new Date(loRowData['cpudt'])
						laParams.push(lcCpudt)
						break ;
					case 'Monat' :
						lcSql += ' v.buper = ? '
						loRowData = laRows[i] ;
						/**@type{String} */
						var lcMonat = loRowData['buper'] ;
						laParams.push(utils.stringToNumber(lcMonat)) ;
						break ;
					
					case 'Jahr' :
						lcSql += ' v.gjahr = ? '
						loRowData = laRows[i] ;
						var lcGjahr = loRowData['gjahr'] ;
						laParams.push(lcGjahr) ;
						break ;
				}
			}
			lcSql += ') '
			

			switch (lcAufriss){
			case 'Rechnung' :
				lcSql += 'group by 1,2,3,4 ' ;
				break ;
			case 'RechnungsPos' :
				lcSql += 'group by 1,2,3,4,5,6,7,8,9,10,11 '
				break ;
			case 'Kunde' :
				lcSql += 'group by 1,2 '
				break ;
			case 'VAD' :
				lcSql += 'group by 1 '
				break ;
		}
			lcSql += 'order by netwr desc '
			break ;
	case 'offen' :
//			switch (lcPeriode){
//			case 'Tag' :
//				lcSql += 'v.cpudt , '
//				break ;
//			case 'Monat' :
//				lcSql += 'LPAD(CAST(v.buper as CHAR),2,"0") buper ,' ;
//				break ;
//			case 'Jahr' :
//				lcSql += 'v.gjahr ,' ;
//				break ;
//			}	
//			lcSql += 'sum(v.netwr) as netwr ' ;
//			lcSql += 'from mes_vkumsatz_open v ' ;
//			
//			lcSql += 'WHERE v.gjahr = ? '				
//			lcSql += 'AND v.netwr <> ? '
//				
//			if (!(lcBuper == null || lcBuper == 0)){
//				lcSql += 'and v.buper = ? '
//				laParams.push(lcBuper)
//			}
//			if (lcKbezirk && lcKbezirk.length > 0){
//				lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
//				laParams.push(lcKbezirk)
//			}
//			
//			lcSql += 'group by 1 '
//			lcSql += 'order by 1 '
//			break ;
	default :
//			lcSql = 'select cpudt , gjahr, lLPAD(CAST(v.buper as CHAR),2,"0") buper , sum(netwr) as netwr from ' 
//			
//			lcSql += '(('
//			lcSql += 'select v.cpudt ,' ;
//			lcSql += 'v.gjahr ,' ;
//			lcSql += 'v.buper ,' ;
//			lcSql += 'sum(v.netwr) as netwr ' ;
//			lcSql += 'from mes_vkumsatz v ' ;
//			
//			lcSql += 'WHERE v.gjahr = ? '				
//			lcSql += 'AND v.netwr <> ? '
//				
//			if (!(lcBuper == null || lcBuper == 0)){
//				lcSql += 'and v.buper = ? '
//				laParams.push(lcBuper)
//			}
//			
//			if (lcKbezirk && lcKbezirk.length > 0){
//				lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
//				laParams.push(lcKbezirk)
//			}
//			lcSql += 'group by v.cpudt,v.gjahr,v.buper '
//			lcSql += 'order by v.cpudt,v.gjahr,v.buper '	
//			lcSql += ') '
//			
//			laParams.push(lcBujahr) ;
//			laParams.push(0) ;
//			
//			lcSql += ' UNION ALL '
//			lcSql += '( select v2.cpudt ,' ;
//			lcSql += 'v2.gjahr ,' ;
//			lcSql += 'LPAD(CAST(v2.buper as CHAR),2,"0") buper ,' ;
//			lcSql += 'sum(v2.netwr) as netwr ' ;
//			lcSql += 'from mes_vkumsatz_open v2 ' ;
//
//			lcSql += 'WHERE v2.gjahr = ? '				
//			lcSql += 'AND v2.netwr <> ? '
//				
//			if (!(lcBuper == null || lcBuper == 0)){
//				lcSql += 'and v2.buper = ? '
//				laParams.push(lcBuper)
//			}
//			
//
//			if (lcKbezirk && lcKbezirk.length > 0){
//				lcSql += 'AND v2.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
//				laParams.push(lcKbezirk)
//			}
//			
//			lcSql += ' group by v2.cpudt,v2.gjahr,v2.buper '
//			lcSql += ' order by v2.cpudt,v2.gjahr,v2.buper '	
//			lcSql += ') '
//			lcSql += ') t group by cpudt, gjahr, buper '
//			lcSql += ' order by cpudt, gjahr, buper '
//			break ;
	}

	application.output(lcSql)
	application.output(laParams)
	
	var loDs2 = databaseManager.getDataSetByQuery('mes',lcSql,laParams,-1) ;

		
	var loGrid = elements.datasettable_1 ;	

	loGrid.rowStyleClassFunc = "(function rowRenderTag2(rowIndex, rowData, field, columnData, event) { if(rowData==null){return 'custom_gridrow_item_small'};return 'custom_gridrow_item_small';})" ;
	
	
	
	var loGridOpt = new Object()
	loGridOpt.rowGroup = true ;
	loGridOpt.groupIncludeTotalFooter = true ; 
	loGridOpt.rowGroupPanelShow = true ;
	loGrid.gridOptions = loGridOpt ;

	elements.datasettable_1.columns = []

	

	
	
	switch (lcAufriss){
	case 'Rechnung' :
		var loCol = add_col('vbeln','Rechnung','STRING',120)
		loCol = add_col('kunnr','KndNr','STRING',110)
		loCol = add_col('kunde_name','Name Kunde','STRING',300)
		break ;
	case 'RechnungsPos' :
		loCol = add_col('vbeln','Rechnung','STRING',120)
		loCol = add_col('posnr','Pos','NUMBER',80)
		loCol = add_col('kunnr','Kdnr','STRING',100)
		loCol = add_col('kunde_name','Name Kunde','STRING',200)
		loCol = add_col('matnr','Material','STRING',110)
		loCol = add_col('mindx','Idx','STRING',60)
		loCol = add_col('mat_bezeich','Bezeichnung','STRING',300)
		loCol = add_col('txt_0001','Text1','STRING',300)
		loCol = add_col('kwmeng','Auftrag','NUMBER',110)
		loCol.cellRendererFunc   = "(function cellRenderTag2(rowIndex, rowData, field, columnData, event) {if(columnData){return '<span>'+columnData.toLocaleString('de-DE',{style:'decimal',minimumFractionDigits:0,maximumFractionDigits:0})+'</span>'}})"
		loCol.cellStyleClassFunc = "(function getNumberStyle2(rowIndex, rowData, field, columnData, event){if(rowData){if(columnData < 0){return 'custom_label_right_red'}};return 'custom_label_right'})" 
		loCol = add_col('aumng','Bezogen','NUMBER',110)
		loCol.cellRendererFunc   = "(function cellRenderTag2(rowIndex, rowData, field, columnData, event) {if(columnData){return '<span>'+columnData.toLocaleString('de-DE',{style:'decimal',minimumFractionDigits:0,maximumFractionDigits:0})+'</span>'}})"
		loCol.cellStyleClassFunc = "(function getNumberStyle2(rowIndex, rowData, field, columnData, event){if(rowData){if(columnData < 0){return 'custom_label_right_red'}};return 'custom_label_right'})" 
		loCol = add_col('kalab','Bestand','NUMBER',110)
		loCol.cellRendererFunc   = "(function cellRenderTag2(rowIndex, rowData, field, columnData, event) {if(columnData){return '<span>'+columnData.toLocaleString('de-DE',{style:'decimal',minimumFractionDigits:0,maximumFractionDigits:0})+'</span>'}})"
		loCol.cellStyleClassFunc = "(function getNumberStyle2(rowIndex, rowData, field, columnData, event){if(rowData){if(columnData < 0){return 'custom_label_right_red'}};return 'custom_label_right'})" 
		break ;
	case 'Kunde' :
		loCol = add_col('kunnr','Kunde','STRING',110)
		loCol = add_col('kunde_name','Name Kunde','STRING',300)
		break ;
	case 'VAD' :
		loCol = add_col('kbezirk','Vertreter','STRING',140)
		break ;
	}
	
	//... Columns zufügen	
	loCol = add_col('netwr','Umsatz','NUMBER',140) ;
	loCol.aggFunc = 'sum' ;
	loCol.pivotIndex = 2
	loCol.cellRendererFunc   = "(function cellRenderTag2(rowIndex, rowData, field, columnData, event) {if(columnData){return '<span>'+columnData.toLocaleString('de-DE',{style:'decimal',minimumFractionDigits:2,maximumFractionDigits:2})+'</span>'}})"
	loCol.cellStyleClassFunc = "(function getNumberStyle2(rowIndex, rowData, field, columnData, event){if(rowData){if(columnData < 0){return 'custom_label_right_red'}};return 'custom_label_right'})" 


	
	
	loGrid.renderData(loDs2)
	
}


/**
 * Fügt dem Grid eine neue Column zu und gibt die Column zurück
 * 
 * @param {String} lcDsField
 * @param {String} lcColBez
 * @param {String} lcFormatType  // DATETIME, NUMBER, STRING
 * @param {Number} lnWidth 
 * @return {CustomType<aggrid-datasettable.column>} loCol
 *
 * @properties={typeid:24,uuid:"C3DA5A7D-23F4-409A-8DDE-0ED5C7B8EF76"}
 */
function add_col(lcDsField,lcColBez,lcFormatType,lnWidth){
	var loCol = elements.datasettable_1.newColumn(lcDsField)
	loCol.dataprovider 		= lcDsField ;
	loCol.headerTitle 		= lcColBez ;
	loCol.enablePivot 		= true ;
	loCol.enableRowGroup 	= true ;
	loCol.formatType 		= lcFormatType ;
	loCol.width 			= lnWidth ;
	loCol.minWidth 			= Math.abs(lnWidth*0.5) ;
	loCol.maxWidth 			= lnWidth * 2 ;
	loCol.enableToolPanel = true ;
	loCol.cellRendererFunc = "(function cellRenderTag(rowIndex, rowData, field, columnData, event) { if(columnData == null){return ''};  return '<span>' +columnData+'<span>' })"
	
	return loCol ;
}


/**
 * Called when the mouse is clicked on a row/cell.
 *
 * @param {object} rowData
 * @param {boolean} selected
 * @param {JSEvent} [event]
 *
 * @properties={typeid:24,uuid:"EF28E51C-6816-47F8-9641-B3BF6838CC40"}
 */
function onRowSelected(rowData, selected, event) {
	if (rowData == null){
		return ;
	}
	if (elements.datasettable_1.getSelectedRows().length==1){
		forms.frm_tagesinfo_pdf.get_dokument()		
	}
//	if (selected == true){
//		forms.frm_tagesinfo_pdf.get_dokument(rowData['vbeln'])		
//	}


}

/**
 * Perform the element onclick action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"11C25794-1FD7-4D63-BE92-495ABCCC9A27"}
 */
function onClickShowPdf(event) {
	forms.frm_tagesinfo_pdf.get_dokument()	
}

/**
 * Perform the element onclick action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"704BBB53-72D0-4BB3-B76B-C15A95ADF265"}
 */
function onClick_selectRows(event) {
	elements.datasettable_1.exportData('Rechnungen.xlsx')
}

/**
 * @param oldValue
 * @param newValue
 * @param {JSEvent} event
 *
 * @return {boolean}
 *
 * @properties={typeid:24,uuid:"FC95EDA1-B30B-4426-B1A1-766AA93EE401"}
 */
function onDataChange(oldValue, newValue, event) {
	SqlBau()
	return true;
}
