/**
 * @type {Date}
 * @properties={typeid:35,uuid:"904FD5CA-D582-48EF-B1B1-7622B1248292",variableType:93}
 */
var ldVonDat = null ;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"BCFC1914-084F-444C-928E-CBF2DEFBE2E3",variableType:93}
 */
var ldBisDat = null ;


/**
 * @type {String}
 * @properties={typeid:35,uuid:"0741B179-53CB-4F71-A1DF-B1EC1502366A"}
 */
var lcKbezirk = null ;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"D0AB1765-DD4E-47DD-B75B-B3974E1D497D"}
 */
var lcBuper = null ;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"B9CA254D-5E16-42D1-A842-4FCC5587F4F0"}
 */
var lcBujahr = null ;

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B2D1E91E-869D-48CE-A6A1-AD919C123C00"}
 */
function onShow(firstShow, event) {
	if (firstShow == true){
		var ldAktDate = new Date() ;
		lcBuper = (ldAktDate.getMonth() + 1).toString()  ;
		lcBujahr = (ldAktDate.getFullYear().toString()) ;
		forms.frm_tagesinfo_grid_01.Sqlbau()
		set_tabposition()
	}
}


/**
 * @properties={typeid:24,uuid:"B9E1A4BC-83E1-4B3E-BB95-5B581266D0AB"}
 */
function set_tabposition(){
	elements.tabpanel_1.dividerLocation = 0.7 ;
	elements.tabpanel_1.dividerSize = 10 ;
	
}



/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D101241C-606A-440E-86DA-0274A3D65975"}
 */
function onLoad(event) {
	
}

/**
 * Perform the element onclick action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8529A589-DCC9-45FD-BD62-236C558C44C1"}
 */
function onAction_refresh(event) {
	forms.frm_tagesinfo_grid_01.Sqlbau()
}

/**
 * Handle changed data, return false if the value should not be accepted. In NGClient you can return also a (i18n) string, instead of false, which will be shown as a tooltip.
 *
 * @param {String} oldValue old value
 * @param {String} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @return {Boolean}
 *
 * @properties={typeid:24,uuid:"87B183A2-EC82-417A-9632-6780F10D771D"}
 */
function onDataChange(oldValue, newValue, event) {
	forms.frm_tagesinfo_grid_01.Sqlbau()
	return true
}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"742A1EE9-C5BC-47D3-8C4E-231A866521EE"}
 */
function onResize(event) {
	elements.tabpanel_1.dividerLocation = 0.7 ;
}
