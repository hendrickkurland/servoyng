
/**
 * Callback method for when solution is opened.
 * When deeplinking into solutions, the argument part of the deeplink url will be passed in as the first argument
 * All query parameters + the argument of the deeplink url will be passed in as the second argument
 * For more information on deeplinking, see the chapters on the different Clients in the Deployment Guide.
 *
 * @param {String} arg startup argument part of the deeplink url with which the Client was started
 * @param {Object<Array<String>>} queryParams all query parameters of the deeplink url with which the Client was started
 *
 * @properties={typeid:24,uuid:"78FB9775-6A09-471C-BDC6-296168DEA365"}
 */
function onSolutionOpen(arg, queryParams) {
	set_public_valuelist()
}



/**
 * @properties={typeid:24,uuid:"56D8D8A2-8A8C-42B2-B6B2-7F8D614B4B2D"}
 */
function set_public_valuelist(){
	fill_valuelist_kbezirk() ;	
	fill_valulist_bujahr() ;
}



/**
 * Valuelist mit VAD-Mitarbeitern
 * 
 * @properties={typeid:24,uuid:"83B07F33-DF97-4852-9661-CFE09FD0E21D"}
 */
function fill_valuelist_kbezirk() {
	var lcSql = 'SELECT distinct '
	lcSql += 'concat(p.name," ",p.first_name) as name ,'	
	lcSql += 'CAST(b.person_id as CHAR) as person_id '	
	lcSql += 'from mes_kbezirk b ' 
	lcSql += 'inner join sys_mes.persons p on b.person_id = p.id '
	lcSql += 'where (b.inaktiv is null or b.inaktiv = 0) '
    lcSql += 'order by name ' ;
		
	var loDs = databaseManager.getDataSetByQuery('mes',lcSql,null,-1) ;
		
	application.setValueListItems('val_vad',loDs.getColumnAsArray(1),loDs.getColumnAsArray(2))

}


/**
 * Valuelist mit Buchungsjahren 
 * @properties={typeid:24,uuid:"D9C8370E-A1BB-4A43-921E-FA0166E90CE0"}
 */
function fill_valulist_bujahr(){
	
	var laBujahr = [] ;
	var ldAkt = new Date()
	laBujahr.push((ldAkt.getFullYear()-0).toString())
	laBujahr.push((ldAkt.getFullYear()-1).toString())
	laBujahr.push((ldAkt.getFullYear()-2).toString())
	laBujahr.push((ldAkt.getFullYear()-3).toString())
	laBujahr.push((ldAkt.getFullYear()-4).toString())
	application.setValueListItems('val_bujahr',laBujahr)
	
}



/**
 * CellStyle
 * @param rowIndex
 * @param rowData
 * @param field
 * @param columnData
 * @param event
 *
 * @properties={typeid:24,uuid:"58AA8C27-B98C-4AF1-9640-02DCD35F805E"}
 */
function cellStyleClassFuncDayColor(rowIndex, rowData, field, columnData, event) {
	   if (!columnData) {
	      return "";
	   }
	   if (columnData) {
	      switch (columnData) {
	      case "Montag":
	         return "label-tag text-info";
	         break;
	      case "Dienstag":
	         return "label-tag text-success";
	         break;
	      case "Mittwoch":
	         return "label-tag text-info";
	         break;
	      default:
	         break;
	      }
	   }
	   return "label-tag text-info";
}
