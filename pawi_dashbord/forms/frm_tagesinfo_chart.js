
/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"5BC62960-9453-4C6F-A0E8-0C88CF390F53"}
 */
function onShow(firstShow, event) {

}
