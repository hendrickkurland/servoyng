/**
 * @type {Array<String>} 
 * @properties={typeid:35,uuid:"7D90B28F-1DAE-4665-A834-C75ECC3C1CB1",variableType:-4}
 */
var laPdf = []



/**
 * Ermittelt URL für Anzeige Faktura
 *
 * @properties={typeid:24,uuid:"3800C93E-243C-41B4-B2ED-42169A4DBED7"}
 */
function get_dokument(){
	
	//... Bereinigen Temp.Dateien- Dateien welche selbst erstellt wurden
	for (var i = 0 ; i < laPdf.length ; i++ )		{
		loFile = plugins.file.convertToRemoteJSFile(laPdf[i])
		if (loFile){
			if (loFile.lastModified() <= ldDate){
				loFile.deleteFile() ;
			}
		}
	}
	laPdf = [] ;
	
	//... Bereinigen Temp.Dateien - Dateien welche liegen geblieben sind (= älter 60 Minuten)
	var laRfiles = plugins.file.getRemoteFolderContents('/temp/','.pdf')
	var ldDate = scopes.svyDateUtils.addMinutes(new Date,-60)
	for (i = 0 ; i < laRfiles.length ; i++ )		{
		var loFile = laRfiles[i] ;
		if (loFile){
			if (loFile.lastModified() <= ldDate){
				loFile.deleteFile() ;
			}
		}
	}

	
	
	
	
	//... Abfrage selektierte PDF - es können mehrere sein
	var laSelectedPdf = forms.frm_tagesinfo_grid_02.elements.datasettable_1.getSelectedRows() ;
	if (laSelectedPdf.length == 0){
    	elements.pdf_Viewer_1.documentURL = '' ;
    	elements.pdf_Viewer_1.reload()
    	return ;
	}
	

	plugins.svyBlockUI.show('Bitte Warten .. Abfrage PDF-Dokumente läuft',1000)
	
		
	var query = "SELECT distinct  " ;
    query += "	v.dokuid, v.verknid " ;
    query += "FROM " ;
    query += "	mes.mes_dokumente_verkn v " ;
    query += "	inner join mes_dokumente d " ;
    query += "	on v.dokuid = d.dokuid " ;
    query += "where " ;
    query += "	v.dokuobj = ? " ;
    query += "  and d.dokutyp = ? " ;
    query += "	and v.verknid = ? " ;
	
    var lcUUID = application.getUUID().toString()
    var loRemoteFile = plugins.file.convertToRemoteJSFile('/temp/'+lcUUID +'.pdf')
    var laBytes = []
	for (var i = 0 ; i < laSelectedPdf.length ; i++ )	{
		var loRow = laSelectedPdf[i];
		
		//... Abfrage Dokuid zu Vertriebsbeleg
		var lcVbeln = loRow['vbeln'] ;
		lcVbeln = utils.stringToNumber(lcVbeln).toString() ;
		var laArgs = ['SAP_FAKT','SAP_FAK',lcVbeln]
		var loDs = databaseManager.getDataSetByQuery('mes',query,laArgs,1)
		
		//... Abfragen Byte zu PDF-Dokument von Middleware
		if (loDs.getMaxRowIndex() >= 0){
			var ladByte =  get_byte_for_dokuid(loDs.getValue(1,1))
			laBytes.push(ladByte)			
		}		
	}
	//... Zusammenführen mehrerer PDF-Files (sofern mehrere Ausgewählt wurden)
	if (laBytes.length == 0){
		return ;
	}
	var loByteNew = plugins.pdf_output.combinePDFDocuments(laBytes)
	
	//... Sichern PDF auf Application.-Server
	loRemoteFile.setBytes(loByteNew,true)	
	var lcRUrl = plugins.file.getUrlForRemoteFile('/temp/'+lcUUID +'.pdf')
	if(lcRUrl) {
		elements.pdf_Viewer_1.documentURL = lcRUrl
	}
	
    	
	//... Sichern PDF-File
	laPdf.push('/'+lcUUID +'.pdf')
	plugins.svyBlockUI.stop() ;
    
}



/**
 * Liest von Middleware das Dokument über HTTP-GET
 * 
 * @param {String} lcDokuId
 * @return {Array<byte>}
 *
 * @properties={typeid:24,uuid:"DA1089CE-4522-4F1E-8746-BA937FE09442"}
 */
function get_byte_for_dokuid(lcDokuId){
	application.output(lcDokuId)
    var lcUrl = 'https://eai.pawi.com:8443/PAWI_Middleware/GET_SINGLE_DOCUMENT?dokuid='+lcDokuId+'&download=1&thumbnail=0'
	var loClient = plugins.http.createNewHttpClient()
	var loRequest = loClient.createGetRequest(lcUrl)
	var loReturn = loRequest.executeRequest()
	if (loReturn.getStatusCode() == 200){
		var loByte = loReturn.getMediaData()
		return loByte ;
	} 
	return null ;	
}