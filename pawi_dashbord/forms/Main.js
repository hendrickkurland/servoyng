/**
 * @properties={typeid:24,uuid:"35065F98-9C9C-4538-A6CA-14FF9BC306D9"}
 * @override
 */
function loadMenuItems() {
	var menuItems = [];

	/** @type {CustomType<servoyextra-sidenav.MenuItem>} */
	var menuItem = new Object();
	menuItem.id = "frm_tagesinfo";
	menuItem.text = "  Tagesinfo"
	menuItem.iconStyleClass = "fa fa-th-large";
	menuItems.push(menuItem);

	// Customer page
	menuItem = new Object();
	menuItem.isDivider = true ;
	menuItems.push(menuItem);
		
	// Order page
	menuItem = new Object();
	menuItem.id = "frm_settings";
	menuItem.text = "Einstellungen"
	menuItem.iconStyleClass = "icon-box";
	menuItems.push(menuItem);

        // return the menu items
	return menuItems;
}


/**
 * @properties={typeid:24,uuid:"24A9364B-31FF-477D-A970-F28E1E1F4FC4"}
 * @override
 */
function loadNavbarItems() {
	var menuItems = [];

	elements.navbar.brandText = 'PAWI Dashboard'
	
        // add default global search
	/** @type {CustomType<bootstrapextracomponents-navbar.menuItem>} */
	var menuItem = elements.navbar.createMenuItem('Search', DEFAULT_NAVBAR_ACTIONS.SEARCH, 'RIGHT');
	menuItem.displayType = 'INPUT_GROUP';
	menuItem.inputButtonStyleClass = "btn-default";
	menuItem.iconName = "fa fa-search";
	menuItems.push(menuItem);

	// user menu item
	menuItem = elements.navbar.createMenuItem('User', 'USER-SETTINGS', 'RIGHT');
	menuItem.displayType = 'MENU_ITEM';
	menuItem.iconName = 'fas fa-user';

        // add a sub-item
	var logoutMenuItem = elements.navbar.createMenuItem('Logout', 'LOGOUT');
	menuItem.subMenuItems = [logoutMenuItem];
	menuItems.push(menuItem);

	return menuItems;
}

// handle the custom actions event for the navbar
/**
 * TODO generated, please specify type and doc for the params
 * @param event
 * @param menuItem
 *
 * @properties={typeid:24,uuid:"CB24AA53-F42C-42D7-B1C3-612E037AB4FD"}
 * @override
 */
function onNavbarMenuItemClicked(event, menuItem) {

	switch (menuItem.itemId) {
	case 'LOGOUT':
		scopes.svySecurity.logout();
		break;
	default:
		break;
	}
}