/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F4FC8A6B-378C-46CC-BCF1-A73F819A7048"}
 */
var lcPeriode = 'Tag'


/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F796718C-5039-4C99-A586-300B0220246E"}
 */
var lcOffen = 'gebucht' ;




/**
 * @properties={typeid:24,uuid:"E8E2BCF4-6426-45CF-9E83-C50D76666F85"}
 */
function setChart(){
	
	var ldAktDate = new Date()
	var lnBuper = ldAktDate.getMonth() + 1 ;
	var lnGjahr = ldAktDate.getFullYear() -4 ;
	
	
	var loParentForm = forms.frm_tagesinfo
	var lcKbezirk = loParentForm.lcKbezirk ;

	
	/**@type{Array<*>} */
	var laParams = [lnGjahr,lnBuper,0]
	
	//... Abfrage Umsatz pro Jahr 
	var lcSql = 'select ' ;
	
	switch (lcOffen){
	case 'gebucht' :		 	
		 	lcSql += 'v.gjahr ,' ;	
			lcSql += 'sum(v.netwr) as netwr ' ;
			lcSql += 'from mes_vkumsatz v ' ;
			lcSql += 'WHERE v.gjahr >= ? '
			lcSql += 'and v.buper = ? '
			lcSql += 'AND v.netwr <> ? '			
			if (lcKbezirk && lcKbezirk.length > 0){
				lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
				laParams.push(lcKbezirk)
			}
			lcSql += 'group by 1 '
			lcSql += 'order by 1 '
			break ;
	case 'offen' :			
			lcSql += 'v.gjahr ,' ;	
			lcSql += 'sum(v.netwr) as netwr ' ;
			lcSql += 'from mes_vkumsatz_open v ' ;
			lcSql += 'WHERE v.gjahr >= ? '
			lcSql += 'AND v.buper = ? '
			lcSql += 'AND v.netwr <> ? '			
			if (lcKbezirk && lcKbezirk.length > 0){
				lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
				laParams.push(lcKbezirk)
			}
			lcSql += 'group by 1 '
			lcSql += 'order by 1 '	

			break ;
	default :
			lcSql = 'select t.gjahr , sum(t.netwr) as netwr from ' 
			
			lcSql += '(('
			lcSql += 'select '
			lcSql += 'v.gjahr ,' ;
			lcSql += 'sum(v.netwr) as netwr ' ;
			lcSql += 'from mes_vkumsatz v ' ;
			lcSql += 'WHERE v.gjahr >= ? '
			lcSql += 'AND v.buper = ? '
			lcSql += 'AND v.netwr <> ? '			
			if (lcKbezirk && lcKbezirk.length > 0){
				lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
				laParams.push(lcKbezirk)
			}
			lcSql += ' group by 1 '
			lcSql += ') '
		

			laParams.push(lnGjahr) ;
			laParams.push(lnBuper) ;
			laParams.push(0) ;
			
			lcSql += ' UNION ALL '
			lcSql += '( select '
			lcSql += 'v2.gjahr ,' ;
			lcSql += 'sum(v2.netwr) as netwr ' ;
			lcSql += 'from mes_vkumsatz_open v2 ' ;

			lcSql += 'WHERE v2.gjahr >= ? '
			lcSql += 'AND v2.buper = ? '
			lcSql += 'AND v2.netwr <> ? '	

			if (lcKbezirk && lcKbezirk.length > 0){
				lcSql += 'AND v2.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
				laParams.push(lcKbezirk)
			}			
			lcSql += ' group by 1 '
			lcSql += ')) t '
			lcSql += ' group by gjahr '
			lcSql += ' order by gjahr '
			break ;
	}
	

	
	var loDs = databaseManager.getDataSetByQuery('mes',lcSql,laParams,-1)
	
	var loData = new Object()
	loData.type = 'bar' ;
	loData.data = new Object() ;
	loData.data.labels = loDs.getColumnAsArray(1) ;
	loData.data.datasets = [] ;
	
	/**@type{Object<*>}*/
	var loGridData = new Object() ;
	loGridData.backgroundColor = [] ;
	loGridData.label = 'Jahre' ;
	loGridData.data = loDs.getColumnAsArray(2) ;
	

	
	for ( var i = 0 ; i < loDs.getMaxRowIndex() ; i++ )	{
		if (loDs.getValue(i+1,1)==ldAktDate.getFullYear()){
			loGridData.backgroundColor.push('#0080c0')
		} else {
			loGridData.backgroundColor.push('#9fffff')
		}
	}	
	

		
	
	loData.data.datasets.push(loGridData)
	forms.frm_tagesinfo_chart_01.elements.chart_1.setData(loData)
	
	
	//... Ermittlung Umsatz pro Tag
	if (loParentForm.lcBuper == null || loParentForm.lcBuper == ''){
		lnBuper = '' ;
	} else {
		lnBuper = utils.stringToNumber(loParentForm.lcBuper) ;
	}

	if (loParentForm.lcBujahr == null || loParentForm.lcBujahr == ''){
		lnGjahr = ldAktDate.getFullYear() ;
	} else {
		lnGjahr = utils.stringToNumber(loParentForm.lcBujahr) ;
	}
	
	laParams = [lnGjahr-1,lnGjahr,lnGjahr-1,0]
	
	lcSql = 'select DAYOFMONTH(v.cpudt) as tag ,'	
	lcSql += 'month(v.cpudt) as monat, '
	lcSql += 'sum(if(v.gjahr=?,v.netwr,0)) as umsatz_tag_vj,'
	lcSql += 'sum(if(v.gjahr=?,v.netwr,0)) as umsatz_tag_aj,'
	lcSql += '0 as umsatz_kum '
	lcSql += 'from mes_vkumsatz v '
	lcSql += 'where v.gjahr >= ? '	
	lcSql += 'and v.netwr <> ? '
	if (lnBuper){
		lcSql += 'and v.buper = ? ' ;
		laParams.push(lnBuper) ;
	}	
	if (lcKbezirk && lcKbezirk.length > 0){
		lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
		laParams.push(lcKbezirk)
	}
	lcSql += 'group by 1,2 '
	lcSql += 'order by 2,1 '
	loDs = databaseManager.getDataSetByQuery('mes',lcSql,laParams,-1)

	
	
	
	var lnKum1 = 0 ;
	var lnKum2 = 0 ;
	var laLabels = [] ;
	var laUmsatzVj = [] ;
	var laUmsatzAj = [] ;

	for ( i = 0 ; i < loDs.getMaxRowIndex() ; i++ ){
		lnKum1 += loDs.getValue(i+1,3) ;
		lnKum2 += loDs.getValue(i+1,4) ;
		laLabels.push(loDs.getValue(i+1,1)) ;
		laUmsatzVj.push(Math.round(lnKum1)) ; // Vorjahr
		laUmsatzAj.push(Math.round(lnKum2)) ; // Aktuelles Jahr
	}
	
	
	loData = new Object()
	loData.type = 'line' ;
	
	loData.data = new Object() ;
	loData.data.labels = laLabels ;
	loData.data.datasets = [] ;
	
	loGridData = new Object() ;
	loGridData.label = lnGjahr ; 
	loGridData.data = laUmsatzAj ;
	loGridData.backgroundColor = ["rgba(70,191,189, 0.2)"]	;
	loGridData.borderColor = '#004080' ;
	loData.data.datasets.push(loGridData)
	
	loGridData = new Object() ;
	loGridData.label = (lnGjahr-1)  ;
	loGridData.data = laUmsatzVj ;
	loGridData.fill = false ;	
	loGridData.borderColor =  '#ff8080' ;
	loData.data.datasets.push(loGridData)
	
	
	forms.frm_tagesinfo_chart_02.elements.chart_1.setData(loData)
	
}


/**
 * Abfrage durchführen
 * @properties={typeid:24,uuid:"528A66CF-42D7-477E-8999-AF591C0A87F0"}
 * @AllowToRunInFind
 */
function Sqlbau(){
		
		setChart() ;
		
		var loParentForm = forms.frm_tagesinfo
		var lcKbezirk = loParentForm.lcKbezirk ;
		var lcBuper = loParentForm.lcBuper ;
		var lcBujahr = loParentForm.lcBujahr ;
				
		var laParams = [lcBujahr,0] ;
		
		
		var lcSql = 'select ' ;
		
		switch (lcOffen){
		case 'gebucht' :
				
		
			 	switch (lcPeriode){
			 	case 'Tag' :
			 		lcSql += 'v.cpudt , '
			 		break ;
			 	case 'Monat' :
			 		lcSql += 'LPAD(CAST(v.buper as CHAR),2,"0") buper ,' ;
			 		break ;
			 	case 'Jahr' :
			 		lcSql += 'v.gjahr ,' ;
			 		break ;
				}	
			

				lcSql += 'sum(v.netwr) as netwr ' ;
				lcSql += 'from mes_vkumsatz v ' ;

				lcSql += 'WHERE v.gjahr = ? '				
				lcSql += 'AND v.netwr <> ? '
					
				if (!(lcBuper == null || lcBuper == 0)){
					lcSql += 'and v.buper = ? '
					laParams.push(lcBuper)
				}
				
				if (lcKbezirk && lcKbezirk.length > 0){
					lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
					laParams.push(lcKbezirk)
				}
				

				lcSql += 'group by 1 '
				lcSql += 'order by 1 '
				break ;
		case 'offen' :
				switch (lcPeriode){
				case 'Tag' :
					lcSql += 'v.cpudt , '
					break ;
				case 'Monat' :
					lcSql += 'LPAD(CAST(v.buper as CHAR),2,"0") buper ,' ;
					break ;
				case 'Jahr' :
					lcSql += 'v.gjahr ,' ;
					break ;
				}	
				lcSql += 'sum(v.netwr) as netwr ' ;
				lcSql += 'from mes_vkumsatz_open v ' ;
				
				lcSql += 'WHERE v.gjahr = ? '				
				lcSql += 'AND v.netwr <> ? '
					
				if (!(lcBuper == null || lcBuper == 0)){
					lcSql += 'and v.buper = ? '
					laParams.push(lcBuper)
				}
				if (lcKbezirk && lcKbezirk.length > 0){
					lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
					laParams.push(lcKbezirk)
				}
				
				lcSql += 'group by 1 '
				lcSql += 'order by 1 '
				break ;
		default :
				lcSql = 'select cpudt , gjahr, lLPAD(CAST(v.buper as CHAR),2,"0") buper , sum(netwr) as netwr from ' 
				
				lcSql += '(('
				lcSql += 'select v.cpudt ,' ;
				lcSql += 'v.gjahr ,' ;
				lcSql += 'v.buper ,' ;
				lcSql += 'sum(v.netwr) as netwr ' ;
				lcSql += 'from mes_vkumsatz v ' ;
				
				lcSql += 'WHERE v.gjahr = ? '				
				lcSql += 'AND v.netwr <> ? '
					
				if (!(lcBuper == null || lcBuper == 0)){
					lcSql += 'and v.buper = ? '
					laParams.push(lcBuper)
				}
				
				if (lcKbezirk && lcKbezirk.length > 0){
					lcSql += 'AND v.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
					laParams.push(lcKbezirk)
				}
				lcSql += 'group by v.cpudt,v.gjahr,v.buper '
				lcSql += 'order by v.cpudt,v.gjahr,v.buper '	
				lcSql += ') '
				
				laParams.push(lcBujahr) ;
				laParams.push(0) ;
				
				lcSql += ' UNION ALL '
				lcSql += '( select v2.cpudt ,' ;
				lcSql += 'v2.gjahr ,' ;
				lcSql += 'LPAD(CAST(v2.buper as CHAR),2,"0") buper ,' ;
				lcSql += 'sum(v2.netwr) as netwr ' ;
				lcSql += 'from mes_vkumsatz_open v2 ' ;

				lcSql += 'WHERE v2.gjahr = ? '				
				lcSql += 'AND v2.netwr <> ? '
					
				if (!(lcBuper == null || lcBuper == 0)){
					lcSql += 'and v2.buper = ? '
					laParams.push(lcBuper)
				}
				

				if (lcKbezirk && lcKbezirk.length > 0){
					lcSql += 'AND v2.kbezirk in (select mes_kbezirk.kbezirk from mes_kbezirk where mes_kbezirk.person_id = ?) '
					laParams.push(lcKbezirk)
				}
				
				lcSql += ' group by v2.cpudt,v2.gjahr,v2.buper '
				lcSql += ' order by v2.cpudt,v2.gjahr,v2.buper '	
				lcSql += ') '
				lcSql += ') t group by cpudt, gjahr, buper '
				lcSql += ' order by cpudt, gjahr, buper '
				break ;
		}
		

		

		var loDs = databaseManager.getDataSetByQuery('mes',lcSql,laParams,-1)
			
		loDs.addColumn('bezeich')	
		
		var loGrid = elements.datasettable_1 ;	
		loGrid.rowStyleClassFunc = "(function rowRenderTag(rowIndex, rowData, field, columnData, event) { if(rowData==null){return 'custom_gridrow_total'};return 'custom_gridrow_item';})" ;
		
		var loGridOpt = new Object()
		loGridOpt.groupIncludeTotalFooter = true ; 
		loGridOpt.suppressMovableColumns = true ;
		loGridOpt.rowGroupPanelShow = false ;
		//loGridOpt.setSideBarVisible = false ;		
		loGrid.gridOptions = loGridOpt ;

		
		elements.datasettable_1.columns = []
		
		
		
		switch (lcPeriode){
	 	case 'Tag' :
		 	for ( var i = 0 ; i < loDs.getMaxRowIndex() ; i++ )	{
				loDs.rowIndex = i+1 ;
				/**@type{Date} */
				var ldDate = loDs['cpudt']
				loDs['cpudt'] = ldDate.setHours(0,0,0,0)
				var lnTag = scopes.svyDateUtils.getDayOfWeek(ldDate)
				var lcTag = '' ;
				switch(lnTag){
					case 1 :
						lcTag = 'Sontag' ;
						break ;
					case 2 :
						lcTag = 'Montag' ;
						break ;
					case 3 :
						lcTag = 'Dienstag' ;
						break ;
					case 4 :
						lcTag = 'Mitwoch' ;
						break ;
					case 5 :
						lcTag = 'Donnerstag' ;
						break ;
					case 6 :
						lcTag = 'Freitag' ;
						break ;
					case 7 :
						lcTag = 'Samstag' ;
						break ;
				}
				loDs['bezeich'] = lcTag ;
			}
		 	
			//... Columns zufügen
			var loCol = loGrid.newColumn('cpudt')
			loCol.format = 'dd.MM.yyyy' ;
			loCol.dataprovider = 'cpudt' ;
			loCol.headerTitle = 'Tag'
			loCol.enablePivot = true ;
			loCol.enableRowGroup = true ;
			loCol.enableToolPanel = true ;
			loCol.formatType = 'DATETIME'
			loCol.width = 140 ;
			loCol.maxWidth = 140 ;
			

			
			
			//... Columns zufügen
			loCol = loGrid.newColumn('bezeich')
			loCol.dataprovider = 'bezeich' ;
			loCol.headerTitle = 'Wochentag'
			loCol.enablePivot = true ;
			loCol.enableRowGroup = true ;
			loCol.formatType = 'STRING'
			loCol.width = 150 ;
			loCol.maxWidth = 150 ;
			loCol.cellRendererFunc = "(function cellRenderTag(rowIndex, rowData, field, columnData, event) { if(columnData == null){return '<b>TOTAL</b>'};  return '<span>' +columnData+'<span>' })"
			loCol.enableToolPanel = true ;
			

			
	 		break ;
	 	case 'Monat' :
	 		
		 	for ( i = 0 ; i < loDs.getMaxRowIndex() ; i++ )	{
				loDs.rowIndex = i+1 ;

				var lcMonat = loDs['buper'] 
				switch(lcMonat){
					case '01' :
						loDs['bezeich'] = 'Januar' ;
						break ;
					case '02':
						loDs['bezeich'] = 'Februar' ;
						break ;
					case '03' :
						loDs['bezeich'] = 'März' ;
						break ;
					case '04' :
						loDs['bezeich'] = 'April' ;
						break ;
					case '05' :
						loDs['bezeich'] = 'Mai' ;
						break ;
					case '06' :
						loDs['bezeich'] = 'Juni' ;
						break ;
					case '07' :
						loDs['bezeich'] = 'Juli' ;
						break ;
					case '08' :
						loDs['bezeich'] = 'August' ;
						break ;
					case '09' :
						loDs['bezeich'] = 'September' ;
						break ;
					case '10' :
						loDs['bezeich'] = 'Oktober' ;
						break ;
					case '11' :
						loDs['bezeich'] = 'November' ;
						break ;
					case '12' :
						loDs['bezeich'] = 'Dezember' ;
						break ;
				}
			}
	 	
			//... Columns zufügen
			loCol = loGrid.newColumn('buper')
			loCol.dataprovider = 'buper' ;
			loCol.headerTitle = 'Monat'
			loCol.enablePivot = true ;
			loCol.enableRowGroup = true ;
			loCol.formatType = 'STRING' ;
			loCol.width = 90 ;
			loCol.maxWidth = 90 ;
			loCol.cellRendererFunc = "(function cellRenderTag(rowIndex, rowData, field, columnData, event) { if(columnData == null){return ''};  return '<span>' +columnData+'<span>' })"

			
			//... Columns zufügen
			loCol = loGrid.newColumn('bezeich')
			loCol.dataprovider = 'bezeich' ;
			loCol.headerTitle = 'Monat'
			loCol.enablePivot = true ;
			loCol.enableRowGroup = true ;
			loCol.formatType = 'STRING' ;
			loCol.width = 200 ;
			loCol.maxWidth = 200 ;
			loCol.cellRendererFunc = "(function cellRenderTag(rowIndex, rowData, field, columnData, event) { if(columnData == null){return '<b>TOTAL</b>'};  return '<span>' +columnData+'<span>' })"
	 		
	 		break ;
	 	case 'Jahr' :
		 	//... Columns zufügen
			loCol = loGrid.newColumn('gjahr')
			loCol.dataprovider = 'gjahr' ;
			loCol.headerTitle = 'Jahr'
			loCol.enablePivot = true ;
			loCol.enableRowGroup = true ;
			loCol.formatType = 'STRING'
			loCol.width = 120 ;
			loCol.maxWidth = 120 ;
			loCol.cellRendererFunc = "(function cellRenderTag(rowIndex, rowData, field, columnData, event) { if(columnData == null){return '<b>TOTAL</b>'};  return '<span>' +columnData+'<span>' })"
	 		break ;
		}
		
		
		
		
		//... Columns zufügen
		loCol = loGrid.newColumn('netwr')
		//loCol.format = '##0.00' ;
		loCol.dataprovider = 'netwr' ;
		loCol.headerTitle = 'Umsatz' ;
		loCol.enablePivot = true ;
		loCol.formatType = 'NUMBER' ;
		loCol.aggFunc = 'sum' ;
		loCol.pivotIndex = 1
		loCol.width = 140 ;
		loCol.maxWidth = 140 ;
		loCol.cellRendererFunc   = "(function cellRenderTag(rowIndex, rowData, field, columnData, event) {if(columnData){return '<span>'+columnData.toLocaleString('de-DE',{style:'decimal',minimumFractionDigits:2,maximumFractionDigits:2})+'</span>'}})"
		loCol.cellStyleClassFunc = "(function getNumberStyle(rowIndex, rowData, field, columnData, event){if(rowData){if(columnData < 0){return 'custom_label_right_red'}};return 'custom_label_right'})" 
		loCol.enableToolPanel = true ;

		loGrid.renderData(loDs)


}




/**
 * @param oldValue
 * @param newValue
 * @param {JSEvent} event
 *
 * @return {boolean}
 *
 * @properties={typeid:24,uuid:"3C0226B3-F1C7-4397-87FA-EBFA3FF8E0BB"}
 */
function onDataChange(oldValue, newValue, event) {
	Sqlbau()
	return true;
}





/**
 * Called when the mouse is clicked on a row/cell.
 *
 * @param {object} rowData
 * @param {boolean} selected
 * @param {JSEvent} [event]
 *
 * @properties={typeid:24,uuid:"F305FB53-33A5-487B-9588-86A374CA9387"}
 */
function onRowSelected(rowData, selected, event) {
	forms.frm_tagesinfo_grid_02.SqlBau() ;
	forms.frm_tagesinfo_pdf.elements.pdf_Viewer_1.documentURL = '' ;
	forms.frm_tagesinfo_pdf.elements.pdf_Viewer_1.reload() ;
}
